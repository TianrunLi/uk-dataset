#!/usr/python

def main():
    counter = 0
    vertex = open('vertex', 'w')
    edge = open('edge', 'w')
    for i in open('ascii_data'):
        counter += 1
        if counter % 100000 == 0:
            print (counter)
        li = i.strip().split(' ')
        vertex.write('%s\n' % (li[0]))
        if len(li) == 1:
            continue
        for j in range(1, len(li)):
            edge.write('%s|%s\n' % (li[0], li[j]))
    vertex.close()
    edge.close()

if __name__ == "__main__":
    main()

