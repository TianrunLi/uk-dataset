#!/bin/bash

wget http://chato.cl/webspam/datasets/uk2007/links/uk-2007-05.graph.gz
wget http://chato.cl/webspam/datasets/uk2007/links/uk-2007-05.offsets.gz
wget http://chato.cl/webspam/datasets/uk2007/links/uk-2007-05.properties

gunzip uk-2007-05.graph.gz
gunzip uk-2007-05.offsets.gz

./compile
./run
python gen_vertex_edge.py

